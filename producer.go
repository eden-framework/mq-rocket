package mq_rocket

import (
	"encoding/json"
	"fmt"
	"gitee.com/eden-framework/context"
	"gitee.com/eden-framework/envconfig"
	"gitee.com/eden-framework/logger"
	"gitee.com/eden-framework/mq-rocket/common"
	"gitee.com/eden-framework/mq-rocket/extensions"
	"github.com/apache/rocketmq-clients/golang/v5"
	"github.com/apache/rocketmq-clients/golang/v5/credentials"
	"github.com/sirupsen/logrus"
	"strings"

	context2 "context"
)

type Producer struct {
	EndPoint  string
	AccessKey string
	Secret    envconfig.Password
	Topic     string
	Filters   string

	producer golang.Producer         `env:"-"`
	filters  []common.ProducerFilter `env:"-"`
}

func (p *Producer) Init() {
	var err error
	p.producer, err = golang.NewProducer(
		&golang.Config{
			Endpoint: p.EndPoint,
			Credentials: &credentials.SessionCredentials{
				AccessKey:    p.AccessKey,
				AccessSecret: p.Secret.String(),
			},
		},
		golang.WithTopics(p.Topic),
	)
	if err != nil {
		logrus.Panicf("init producer client failure, err: %v", err)
	}

	// filters
	if p.Filters != "" {
		filterNames := strings.Split(p.Filters, ",")
		for _, filterName := range filterNames {
			filterName = strings.TrimSpace(filterName)
			filter := extensions.MustGetProducerFilter(filterName)
			p.filters = append(p.filters, filter)
		}
	}
}

func (p *Producer) Start(ctx *context.WaitStopContext) {
	ctx.Add(1)
	go func() {
		<-ctx.Done()
		logrus.Infof("[RocketMQProducer] [%s] graceful shutdown...", p.Topic)
		p.producer.GracefulStop()
		logrus.Infof("[RocketMQProducer] [%s] shutdown complete.", p.Topic)
		ctx.Finish()
	}()

	err := p.producer.Start()
	if err != nil {
		logrus.Panicf("start producer[%s] failure, err: %v", p.Topic, err)
	}
}

func (p *Producer) SendSync(ctx context2.Context, message *common.BaseMessage) (string, error) {
	log := logger.GetLogger(ctx)
	if message.GetBody() == nil {
		log.Errorf("[RocketMQProducer] message body is nil")
		return "", fmt.Errorf("message body is nil")
	}
	if len(p.filters) > 0 {
		for _, filter := range p.filters {
			filter(ctx, message)
		}
	}
	msg, err := makeRocketMQMessage(message, p.Topic)
	if err != nil {
		log.Errorf("[RocketMQProducer] makeRocketMQMessage failure, err: %v", err)
		return "", err
	}
	receipts, err := p.producer.Send(ctx, msg)
	if err != nil {
		log.Errorf("[RocketMQProducer] sync send mq message failure, err: %v", err)
		return "", err
	}
	return receipts[0].MessageID, nil
}

func (p *Producer) SendAsync(
	ctx context2.Context,
	message *common.BaseMessage,
	handler common.AsyncSendCallback,
) error {
	log := logger.GetLogger(ctx)
	if message.GetBody() == nil {
		log.Errorf("[RocketMQProducer] message body is nil")
		return fmt.Errorf("message body is nil")
	}
	if len(p.filters) > 0 {
		for _, filter := range p.filters {
			filter(ctx, message)
		}
	}
	msg, err := makeRocketMQMessage(message, p.Topic)
	if err != nil {
		log.Errorf("[RocketMQProducer] makeRocketMQMessage failure, err: %v", err)
		return err
	}
	p.producer.SendAsync(
		ctx, msg, func(ctx context2.Context, receipts []*golang.SendReceipt, err error) {
			if err != nil {
				log.Errorf("[RocketMQProducer] async send mq message failure, err: %v", err)
				handler(ctx, "", err)
				return
			}
			handler(ctx, receipts[0].MessageID, nil)
		},
	)

	return nil
}

func makeRocketMQMessage(message *common.BaseMessage, topic string) (*golang.Message, error) {
	bodyBytes, err := json.Marshal(message.GetBody())
	if err != nil {
		return nil, err
	}

	msg := &golang.Message{
		Topic: topic,
		Body:  bodyBytes,
	}
	if len(message.Keys()) > 0 {
		msg.SetKeys(message.Keys()...)
	}
	if len(message.Properties()) > 0 {
		for key, val := range message.Properties() {
			msg.AddProperty(key, val)
		}
	}
	if message.Tag() != "" {
		msg.SetTag(message.Tag())
	}

	return msg, nil
}
