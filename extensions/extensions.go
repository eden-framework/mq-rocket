package extensions

import (
	"gitee.com/eden-framework/mq-rocket/common"
	"github.com/sirupsen/logrus"
)

var extensions = newExtensions()

type Extensions struct {
	producerFilters map[string]common.ProducerFilter
}

func newExtensions() *Extensions {
	return &Extensions{
		producerFilters: make(map[string]common.ProducerFilter),
	}
}

func RegisterProducerFilter(name string, filter common.ProducerFilter) {
	if _, ok := extensions.producerFilters[name]; ok {
		logrus.Panic("duplicated producer filter name: " + name)
	}
	extensions.producerFilters[name] = filter
}

func GetProducerFilter(name string) common.ProducerFilter {
	if filter, ok := extensions.producerFilters[name]; ok {
		return filter
	}
	logrus.Warnf("producer filter not found: %s", name)
	return nil
}

func MustGetProducerFilter(name string) common.ProducerFilter {
	if filter, ok := extensions.producerFilters[name]; ok {
		return filter
	}
	logrus.Panicf("producer filter not found: %s", name)
	return nil
}
