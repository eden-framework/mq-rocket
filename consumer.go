package mq_rocket

import (
	context2 "context"
	"gitee.com/eden-framework/context"
	"gitee.com/eden-framework/envconfig"
	"gitee.com/eden-framework/mq-rocket/common"
	"github.com/apache/rocketmq-clients/golang/v5"
	"github.com/apache/rocketmq-clients/golang/v5/credentials"
	"github.com/apache/rocketmq-clients/golang/v5/protocol/v2"
	"github.com/sirupsen/logrus"
	"time"
)

type Consumer struct {
	Name      string
	EndPoint  string
	AccessKey string
	Secret    envconfig.Password

	MaxMessageNum     int32
	InvisibleDuration envconfig.Duration
	ReadDuration      envconfig.Duration
	Handler           common.ConsumerHandler `ignored:"true"`

	Topic           string
	SubscriptionSQL string
	Group           string
	Filters         string

	consumer golang.SimpleConsumer
	filters  []common.ProducerFilter
}

func (c *Consumer) Init() {
	var err error
	c.consumer, err = golang.NewSimpleConsumer(
		&golang.Config{
			Endpoint:      c.EndPoint,
			ConsumerGroup: c.Group,
			Credentials: &credentials.SessionCredentials{
				AccessKey:    c.AccessKey,
				AccessSecret: c.Secret.String(),
			},
		},
		golang.WithAwaitDuration(time.Duration(c.ReadDuration)),
		golang.WithSubscriptionExpressions(
			map[string]*golang.FilterExpression{
				c.Topic: golang.NewFilterExpressionWithType(c.SubscriptionSQL, golang.SQL92),
			},
		),
	)
	if err != nil {
		logrus.Panicf("init consumer client failure, err: %v", err)
	}
}

func (c *Consumer) BindHandler(h common.ConsumerHandler) {
	c.Handler = h
}

func (c *Consumer) Start(ctx *context.WaitStopContext) {
	if c.Handler == nil {
		logrus.Panicf("consumer[%s] handler is nil", c.Name)
	}

	ctx.Add(1)
	go func() {
		<-ctx.Done()
		logrus.Infof("[RocketMQConsumer] [%s] graceful shutdown...", c.Name)
		c.consumer.GracefulStop()
		logrus.Infof("[RocketMQConsumer] [%s] shutdown complete.", c.Name)
		ctx.Finish()
	}()

	err := c.consumer.Start()
	if err != nil {
		logrus.Panicf("start consumer[%s] failure, err: %v", c.Name, err)
	}

	go func() {
		logrus.Infof("[RocketMQConsumer] [%s] start success", c.Name)
		for {
			receives, err := c.consumer.Receive(context2.TODO(), c.MaxMessageNum, time.Duration(c.InvisibleDuration))
			if err != nil {
				rpcErr, ok := golang.AsErrRpcStatus(err)
				if !ok {
					logrus.Errorf("[RocketMQConsumer] [%s] receive message fatal err: %v", c.Name, err)
					continue
				}
				if rpcErr.Code != int32(v2.Code_MESSAGE_NOT_FOUND.Number()) {
					logrus.Errorf("[RocketMQConsumer] [%s] receive message failure, err: %v", c.Name, err)
				}
				continue
			}

			for _, receive := range receives {
				if err = c.Handler(context2.Background(), receive); err == nil {
					c.consumer.Ack(context2.Background(), receive)
				} else {
					logrus.Errorf("[RocketMQConsumer] [%s] handle message failure, err: %v", c.Name, err)
				}
			}
		}
	}()
}
