package common

type BaseMessage struct {
	body       any
	keys       []string
	properties map[string]string
	tag        string
}

func NewBaseMessage(body any) *BaseMessage {
	return &BaseMessage{
		body:       body,
		properties: make(map[string]string),
		keys:       make([]string, 0),
	}
}

func (m *BaseMessage) Keys() []string {
	return m.keys
}

func (m *BaseMessage) Properties() map[string]string {
	return m.properties
}

func (m *BaseMessage) Tag() string {
	return m.tag
}

func (m *BaseMessage) GetBody() any {
	return m.body
}

func (m *BaseMessage) AddKeys(keys ...string) *BaseMessage {
	m.keys = keys
	return m
}

func (m *BaseMessage) AddProperty(key, value string) *BaseMessage {
	m.properties[key] = value
	return m
}

func (m *BaseMessage) SetTag(tag string) *BaseMessage {
	m.tag = tag
	return m
}
