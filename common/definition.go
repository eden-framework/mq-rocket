package common

import (
	context2 "context"
	"github.com/apache/rocketmq-clients/golang/v5"
)

type AsyncSendCallback func(ctx context2.Context, messageId string, err error)
type ProducerFilter func(ctx context2.Context, message *BaseMessage)
type ConsumerHandler func(ctx context2.Context, message *golang.MessageView) error
